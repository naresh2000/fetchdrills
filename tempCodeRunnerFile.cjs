
function fetchFirstTodo() {
    return fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch todos');
        }
        return response.json();
      })
      .then(todos => {
        if (todos.length === 0) {
          throw new Error('No todos found');
        }
        return todos[0]; // Get the first todo
      })
      .catch(error => {
        console.error('Error fetching the first todo:', error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  function fetchUserDetails(userId) {
    return fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
      .then(response => {
        if (!response.ok) {
          throw new Error(`Failed to fetch user details for userId: ${userId}`);
        }
        return response.json();
      })
      .catch(error => {
        console.error(`Error fetching user details for userId: ${userId}`, error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  fetchFirstTodo()
    .then(todo => {
      console.log('First Todo:', todo);
      return fetchUserDetails(todo.userId); // Chain the promise to fetch user details for the associated user
    })
    .then(userDetails => {
      console.log('User Details:', userDetails);
      // Proceed with other solutions here
    })
    .catch(error => {
      console.error('Error:', error);
      // Handle the error or display an appropriate message
    });
  
