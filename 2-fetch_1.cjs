function fetchAllUsers() {
    return fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch users');
        }
        return response.json();
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        throw error; 
      });
  }
  
  fetchAllUsers()
    .then(users => {
      console.log('Users:', users);
    
    })
    .catch(error => {
      console.error('Error:', error);
      
    });





function fetchAllTodos() {
    return fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => {
            if (!response.ok) {
              throw new Error('Failed to fetch todos');
            }
            return response.json();
        })
        .catch(error => {
            console.error('Error fetching todos:', error);
            throw error; // Propagate the error to prevent subsequent solutions from executing
        });
       }
      
      fetchAllTodos()
        .then(todos => {
          console.log('Todos:', todos);
          // Proceed with other solutions here
        })
        .catch(error => {
          console.error('Error:', error);
          // Handle the error or display an appropriate message
        });


// 3. Use the promise chain and fetch the users first and then the todos.

function fetchAllUsers() {
    return fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch users');
        }
        return response.json();
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  function fetchAllTodos() {
    return fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch todos');
        }
        return response.json();
      })
      .catch(error => {
        console.error('Error fetching todos:', error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  fetchAllUsers()
    .then(users => {
      console.log('Users:', users);
      return fetchAllTodos(); // Chain the promise to fetch todos
    })
    .then(todos => {
      console.log('Todos:', todos);
      // Proceed with other solutions here
    })
    .catch(error => {
      console.error('Error:', error);
      // Handle the error or display an appropriate message
    });




//Use the promise chain and fetch the users first and then all the details for each user.





function fetchAllUsers() {
    return fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch users');
        }
        return response.json();
      })
      .catch(error => {
        console.error('Error fetching users:', error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  function fetchUserDetails(userId) {
    return fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
      .then(response => {
        if (!response.ok) {
          throw new Error(`Failed to fetch user details for userId: ${userId}`);
        }
        return response.json();
      })
      .catch(error => {
        console.error(`Error fetching user details for userId: ${userId}`, error);
        throw error; // Propagate the error to prevent subsequent solutions from executing
      });
  }
  
  fetchAllUsers()
    .then(users => {
      console.log('Users:', users);
      const userPromises = users.map(user => fetchUserDetails(user.id));
      return Promise.all(userPromises); // Chain the promise to fetch user details for each user
    })
    .then(userDetails => {
      console.log('User Details:', userDetails);
      // Proceed with other solutions here
    })
    .catch(error => {
      console.error('Error:', error);
      // Handle the error or display an appropriate message
    });






  

//  Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function fetchFirstTodo() {
  return fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => {
      if (!response.ok) {
        throw new Error('Failed to fetch todos');
      }
      return response.json();
    })
    .then(todos => {
      if (todos.length === 0) {
        throw new Error('No todos found');
      }
      return todos[0]; // Get the first todo
    })
    .catch(error => {
      console.error('Error fetching the first todo:', error);
      throw error; // Propagate the error to prevent subsequent solutions from executing
    });
}

function fetchUserDetails(userId) {
  return fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
    .then(response => {
      if (!response.ok) {
        throw new Error(`Failed to fetch user details for userId: ${userId}`);
      }
      return response.json();
    })
    .catch(error => {
      console.error(`Error fetching user details for userId: ${userId}`, error);
      throw error; // Propagate the error to prevent subsequent solutions from executing
    });
}

fetchFirstTodo()
  .then(todo => {
    console.log('First Todo:', todo);
    return fetchUserDetails(todo.userId); // Chain the promise to fetch user details for the associated user
  })
  .then(userDetails => {
    console.log('User Details:', userDetails);
    // Proceed with other solutions here
  })
  .catch(error => {
    console.error('Error:', error);
    // Handle the error or display an appropriate message
  });







  





      

  
  
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
